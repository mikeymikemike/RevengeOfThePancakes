using System;
using Xunit;
using RevengeOfThePancakes.App;

namespace RevengeOfThePancakes.Test
{
    public class RevengeOfThePancakesTest
    {
        [Fact]
        public void NormalPancakeStack_ShouldGetCorrectAnswer()
        {
            var ps = new PancakeStack("+--+-");
            Assert.Equal(4, ps.NumberOfFlips);
        }

        [Fact]
        public void MinimalistPancakeStack_ShouldGetCorrectAnswer()
        {
            var mps = new MinimalisticPancakeStack("+--+-");
            Assert.Equal(4, mps.NumberOfFlips);
        }

        [Fact]
        public void NormalPancakeStack_FailWhenGivenInvalidArgument()
        {
            Assert.Throws<ArgumentException>(() => new PancakeStack("+-x-+-"));
        }

        [Fact]
        public void NormalPancakeStack_FailWhenGivenNoArgument()
        {
            Assert.Throws<ArgumentNullException>(() => new PancakeStack("  "));
        }
    }
}
