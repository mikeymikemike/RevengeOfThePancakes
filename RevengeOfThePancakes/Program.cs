﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace RevengeOfThePancakes.App
{
    /// <summary>
    /// Class: PancakeStack
    /// 
    /// NOTE: I know that Go is less Object Oriented than C#, and I'll come to grasp that further as I learn Go,
    /// but C# leans on classes, and so I chose to stick to the paradigm of the language.
    /// </summary>
    public class PancakeStack
    {
        private List<bool> _stack = new List<bool>();
        private int _numberOfFlips = 0;

        public int NumberOfFlips { get { return _numberOfFlips; } }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="inputStr">The combination of +/- that represent the pancake stack</param>
        public PancakeStack(string inputStr)
        {
            ValidateInputString(inputStr);
            ParsePancakeInputStringToBoolArray(inputStr);
            CalculateNumberOfFlipsRequired();
        }

        // NOTE: Not required as part of challenge
        private void ValidateInputString(string inputString)
        {
            if (String.IsNullOrWhiteSpace(inputString))
            {
                throw new ArgumentNullException("No Input String Provided");
            }

            Regex rx = new Regex(@"^[+-]*$", RegexOptions.Compiled);
            MatchCollection matches = rx.Matches(inputString);
            if (!matches.Count.Equals(1))
            {
                throw new ArgumentException($"Invalid pancake input string.  Only a combination of '+' or '-' allowed.  String Provided: {inputString}");
            }
        }

        // NOTE: Booleans are easier to work with than +/-, so convert it to a more code-friendly data type
        private void ParsePancakeInputStringToBoolArray(string input)
        {
            // convert +/- to true/false (true means the pancake is upright)
            foreach (var pancake in input)
            {
                var isUpright = pancake.Equals('+');
                _stack.Add(isUpright);
            }
        }

        // Loop through the pancakes, top to bottom.  If the one below the current pancake doesn't match, 
        // flip the pancakes on top to match the one below the index, even if its upside down.
        public void CalculateNumberOfFlipsRequired()
        {
            for (var i = 1; i < _stack.Count; i++)
            {
                // NOTE: variable name for visibility/clarity.
                var mismatch = !_stack[i - 1].Equals(_stack[i]);

                if (mismatch)
                {
                    _numberOfFlips++;
                }
            }

            // If you get to the bottom and it's upside down, that means the whole stack is upside down, so flip the entire stack.
            // NOTE: I wanted to work this into the loop above, but couldn't do it gracefully.
            // I chose to go for readability and separate this out.
            if (!_stack[_stack.Count - 1])
            {
                _numberOfFlips++;
            }
        }
    }

    // Never would I write a function like this, nor would I name a function like this either, but
    // wanted to have some fun with reducing to the lowest level.  I choose readability and maintainability over 
    // unresonable condensability.
    public class MinimalisticPancakeStack
    {
        public int NumberOfFlips { get; set; } = 0;

        public MinimalisticPancakeStack(string inputStr)
        {
            for (int i = 1; i < inputStr.Length; i++)
            {
                if (inputStr[i] != inputStr[i - 1])
                {
                    NumberOfFlips++;
                }
            }

            if (inputStr[inputStr.Length - 1].Equals('-'))
            {
                NumberOfFlips++;
            }
        }
    }

    /// <summary>
    /// Main Entry Point for Application
    /// </summary>
    class Program
    {
        // Simple Prompt and Read function
        static string GetInput()
        {
            Console.Write("> ");
            return Console.ReadLine();
        }

        // Take in the number of stacks of pancakes provided by the user, and then calculate the number 
        // of flips required for the provided number of stacks
        static void Run()
        {
            int stackCount = int.Parse(GetInput());
            string output = "";

            for (int i = 1; i <= stackCount; i++)
            {
                string inputStr = GetInput();
                var pancakeStack = new PancakeStack(inputStr);
                output += $"Case #{i}: {pancakeStack.NumberOfFlips}\n";

                // This is stupidly condensed.  No function should be this reduced, but wanted to do it for fun.
                // I would never do this in the real world, though.

                /*
                 var minimalistPancakeStack = new MinimalisticPancakeStack(inputStr);
                  output += $"Case #{i}: {minimalistPancakeStack.NumberOfFlips}\n";
                */
            }

            Console.WriteLine(output);
        }

        // NOTE: I try to keep Main light, so I extracted the program to Run().
        static void Main(string[] args)
        {
            Run();
            Console.WriteLine("End of Program.  Press any key to exit...");
            Console.ReadLine();
        }
    }
}
