# Revenge of the Pancakes!
A Code Challenge by Michael Campbell for Weave

## My Approach:
There are two different solutions in this project - the OOP (and realistic) approach, and the Minimalist (and unrealistic, but fun) approach.  

Working in C#, I leaned on the OOP style solution and created a `PancakeStack` class that contains the properties and methods required to solve the challenge.  

Then, I wrote the `MinimalistPancakeStack` for kicks and giggles.  It's not a realistic solution, but is written in the unreasonably condensed versions often found in CodeWars and such.

#### Misc.
Wherever you see a `NOTE:`, that is a clarifying note for this demonstration only, and I wouldn't typically put a comment in like that.

Also, I kept all my classes and program in a single file for readability and to speed up review.